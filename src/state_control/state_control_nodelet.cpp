#include <memory>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

#include "state_control.hpp"

namespace neverdrive_state_control_ros_tool {

class StateControlNodelet : public nodelet::Nodelet {

    inline void onInit() override {
        impl_ = std::make_unique<StateControl>(getNodeHandle(), getPrivateNodeHandle());
    }
    std::unique_ptr<StateControl> impl_;
};
} // namespace neverdrive_state_control_ros_tool

PLUGINLIB_EXPORT_CLASS(neverdrive_state_control_ros_tool::StateControlNodelet, nodelet::Nodelet);
