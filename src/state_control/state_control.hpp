#pragma once

#include <dynamic_reconfigure/server.h>
#include <ros/forwards.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include "neverdrive_state_control_ros_tool/State.h"
//#include <string>

#include "neverdrive_state_control_ros_tool/StateControlInterface.h"

namespace neverdrive_state_control_ros_tool {

class StateControl {

    using Interface = StateControlInterface;

    using Msg = std_msgs::Header;

public:
    StateControl(ros::NodeHandle, ros::NodeHandle);

private:

    void callbackTimer(const ros::TimerEvent&);                       //

    void callbackSubscriber(const Msg::ConstPtr& msg);
    void reconfigureRequest(const Interface::Config&, uint32_t);

    State::Ptr state_{new State};                   //
    ros::Timer timer_;                                            //

    Interface interface_;
    dynamic_reconfigure::Server<Interface::Config> reconfigureServer_;

    tf2_ros::Buffer tfBuffer_;
    tf2_ros::TransformListener tfListener_{tfBuffer_};
    tf2_ros::TransformBroadcaster tfBroadcaster_;


};
} // namespace neverdrive_state_control_ros_tool
