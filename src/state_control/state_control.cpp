#include "state_control.hpp"

namespace neverdrive_state_control_ros_tool {

StateControl::StateControl(ros::NodeHandle nhPublic, ros::NodeHandle nhPrivate)
        : interface_{nhPrivate}, reconfigureServer_{nhPrivate} {

    /**
     * Initialization
     */
    interface_.fromParamServer();

    /**
     * Set up callbacks for subscribers and reconfigure.
     *
     * New subscribers can be created with "add_subscriber" in "cfg/StateControl.if file.
     * Don't forget to register your callbacks here!
     */
    reconfigureServer_.setCallback(boost::bind(&StateControl::reconfigureRequest, this, _1, _2));
    //interface_.dummy_subscriber->registerCallback(&StateControl::callbackSubscriber, this);

    std::string string_tmp = "SIGN_LEFT_DETECTED";   //
    state_->sign_flag = string_tmp;
    state_->task_flag = 2;//
    timer_=nhPrivate.createTimer(ros::Rate(0.3), &StateControl::callbackTimer, this);                  //

    rosinterface_handler::showNodeInfo();
}

    void StateControl::callbackTimer(const ros::TimerEvent & timerEvent) {          //

        interface_.state_publisher.publish(state_);

}



void StateControl::callbackSubscriber(const Msg::ConstPtr& msg) {

    // do your stuff here...
    Msg::Ptr newMsg = boost::make_shared<Msg>(*msg);
    interface_.state_publisher.publish(newMsg);
}

/**
  * This callback is called at startup or whenever a change was made in the dynamic_reconfigure window
*/
void StateControl::reconfigureRequest(const Interface::Config& config, uint32_t level) {
    interface_.fromConfig(config);
}


} // namespace neverdrive_state_control_ros_tool
