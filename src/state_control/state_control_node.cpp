#include "state_control.hpp"

int main(int argc, char* argv[]) {

    ros::init(argc, argv, "state_control_node");

    neverdrive_state_control_ros_tool::StateControl state_control(ros::NodeHandle(), ros::NodeHandle("~"));

    ros::spin();
    return EXIT_SUCCESS;
}
